package com.nikola.zenjob.network.response

import java.io.Serializable

data class Location(
    val city: String,
    val district: String,
    val locationLatitude: Double,
    val locationLongitude: Double,
    val locationName: String,
    val locationSearchString: String,
    val postalCode: String,
    val street: String,
    val streetNumber: String,
    val supplementary: String
):Serializable
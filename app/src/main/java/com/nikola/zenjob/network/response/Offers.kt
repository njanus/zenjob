package com.nikola.zenjob.network.response

data class Offers(
    val offset: Int,
    var max: Int,
    val total: Int,
    val newestTimestamp: Int,
    val offers: ArrayList<Offer> = arrayListOf()
)

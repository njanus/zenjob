package com.nikola.zenjob.network

class ApiConstants {
    companion object {
        const val HTTP_TIMEOUTS: Long = 11// seconds
        const val BASE_URL = "https://staging-main.zenjob.org/"

        // HEADERS
        const val HEADER_KEY_CONTENT_TYPE = "Content-Type"
        const val HEADER_VALUE_CONTENT_TYPE_JSON = "application/json"
        const val HEADER_KEY_USER_TOKEN = "Authorization"
        const val HEADER_KEY_BEARER = "Bearer "


        // ENDPOINTS
        const val AUTH = "api/employee/v1/auth"
        const val OFFSET_OFFERS = "api/employee/v1/offers?offset=0"
        const val OFFER_BY_ID = "api/employee/v1/offers/{id}"
        const val NEW_OFFER = "api/employee/v1/sandbox/offer"

        /// HTTP RESPONSE CODE
        const val HTTP_OK = 200
        const val DEFAULT_ERROR = 400
        const val SUCCESS_AUTH_MAIL: Int = 201

    }

}
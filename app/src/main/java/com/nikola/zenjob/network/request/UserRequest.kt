package com.nikola.zenjob.network.request

data class UserRequest(
    val username: String,
    val password: String
)

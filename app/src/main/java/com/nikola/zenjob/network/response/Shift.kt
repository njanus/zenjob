package com.nikola.zenjob.network.response

import java.io.Serializable

data class Shift(
    val beginDate: String,
    val breakTypes: Int,
    val endDate: String
):Serializable
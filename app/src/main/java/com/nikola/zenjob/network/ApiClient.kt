package com.nikola.zenjob.network

import android.text.TextUtils
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiClient {

    @Synchronized
    fun getRetrofit(token: String?): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(createGsonConverter()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(ApiConstants.BASE_URL)
            .client(createHttpClient(token))
            .build()
    }

    private fun createGsonConverter(): Gson {
        return GsonBuilder()
            .setLenient()
            .create()
    }

    private fun createHttpClient(token: String?): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()

        // setting timeouts and disabling retries
        clientBuilder.retryOnConnectionFailure(true)
            .readTimeout(ApiConstants.HTTP_TIMEOUTS, TimeUnit.SECONDS)
            .writeTimeout(ApiConstants.HTTP_TIMEOUTS, TimeUnit.SECONDS)
            .connectTimeout(ApiConstants.HTTP_TIMEOUTS, TimeUnit.SECONDS).build()

        clientBuilder.addInterceptor(
            createJsonContentTypeAndHeaderInterceptor()
        )
        clientBuilder.addInterceptor(
            createUserTokenInterceptor(token)
        )
        clientBuilder.addInterceptor(
            createHttpLoggingInterceptor()
        )
        return clientBuilder.build()
    }

}


        private fun createUserTokenInterceptor(token: String?): Interceptor {
            return Interceptor { chain ->
                val original = chain.request()
                if (TextUtils.isEmpty(token)) {
                    return@Interceptor chain.proceed(original)
                }

                val request = original.newBuilder()
                    .addHeader(ApiConstants.HEADER_KEY_USER_TOKEN, ApiConstants.HEADER_KEY_BEARER + token)
                    .build()

                return@Interceptor chain.proceed(request)

            }
        }

        private fun createHttpLoggingInterceptor(): Interceptor {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            return httpLoggingInterceptor
        }

        private fun createJsonContentTypeAndHeaderInterceptor(): Interceptor {
            return Interceptor { chain ->
                val original = chain.request()

                val request = original.newBuilder()
                    .addHeader(
                        ApiConstants.HEADER_KEY_CONTENT_TYPE,
                        ApiConstants.HEADER_VALUE_CONTENT_TYPE_JSON
                    )
                    .build()

                chain.proceed(request)
            }
}


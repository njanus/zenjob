package com.nikola.zenjob.network.response

import java.io.Serializable

data class BreakType(
    val description: String,
    val minutes: Int
):Serializable
package com.nikola.zenjob.network.services


import com.nikola.zenjob.network.ApiConstants
import com.nikola.zenjob.network.request.CreateOfferRequest
import com.nikola.zenjob.network.request.UserRequest
import com.nikola.zenjob.network.response.Authorization
import com.nikola.zenjob.network.response.Offer
import com.nikola.zenjob.network.response.Offers
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiService {

    @POST(ApiConstants.AUTH)
    fun login(@Body user: UserRequest?): Single<Authorization>

    @GET(ApiConstants.OFFSET_OFFERS)
    fun getOffers(): Single<Offers>

    @GET(ApiConstants.OFFER_BY_ID)
    fun getOfferById(@Path("id") id: String): Single<Offer>

    @POST(ApiConstants.NEW_OFFER)
    fun createNewOffer(@Body newOffer: CreateOfferRequest): Single<Void>

}
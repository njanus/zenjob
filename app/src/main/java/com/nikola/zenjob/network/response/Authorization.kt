package com.nikola.zenjob.network.response

data class Authorization(
    val username: String,
    var roles: ArrayList<String>,
    val token_type: String,
    val access_token: String,
    val expires_in: Int,
    val refresh_token: String
)


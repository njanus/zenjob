package com.nikola.zenjob.network.response

import java.io.Serializable

data class Offer(
    val breakTypes: List<BreakType> = arrayListOf(),
    val companyLogoUrl: String,
    val companyName: String,
    val description: String,
    val earningHourly: String,
    val earningTotal: String?,
    val hourSum: Double,
    val id: String,
    val instructions: String,
    val jobCategoryKey: String,
    val location: Location,
    val minutesSum: String,
    val pricingTables: List<PricingTable> = arrayListOf(),
    val shifts: ArrayList<Shift> = arrayListOf(),
    val title: String?
) : Serializable {
}
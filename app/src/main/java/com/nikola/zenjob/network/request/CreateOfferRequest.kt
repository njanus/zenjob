package com.nikola.zenjob.network.request

data class CreateOfferRequest(
    val shifts: Int,
    val instructions: Boolean
)
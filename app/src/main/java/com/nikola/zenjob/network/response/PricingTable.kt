package com.nikola.zenjob.network.response

import java.io.Serializable

data class PricingTable(
    val earningHourly: String,
    val earningTotal: String,
    val isSummary: Boolean,
    val minutes: Int,
    val name: String,
    val times: String,
    val unpaid: Boolean
):Serializable
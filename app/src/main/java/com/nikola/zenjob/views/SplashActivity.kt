package com.nikola.zenjob.views

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.nikola.zenjob.R
import com.nikola.zenjob.app.DataConstants.Companion.SPLASH_DELAY_TIME

class SplashActivity : AppCompatActivity() {

    private lateinit var delayHandler: Handler

    private val runnable: Runnable = Runnable {
        if (!isFinishing) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        delayHandler = Handler()
        //Navigate with delay
        delayHandler.postDelayed(runnable, SPLASH_DELAY_TIME)

    }

    public override fun onDestroy() {
        delayHandler.removeCallbacks(runnable)
        super.onDestroy()
    }

}




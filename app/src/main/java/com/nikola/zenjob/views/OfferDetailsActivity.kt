package com.nikola.zenjob.views

import android.content.Context
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.nikola.zenjob.R
import com.nikola.zenjob.app.DataConstants
import com.nikola.zenjob.network.ApiClient
import com.nikola.zenjob.network.request.CreateOfferRequest
import com.nikola.zenjob.network.response.Offer
import com.nikola.zenjob.network.services.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_offer.linearLayoutJobDetails
import kotlinx.android.synthetic.main.item_offer.textViewCity
import kotlinx.android.synthetic.main.item_offer.textViewDistrict
import kotlinx.android.synthetic.main.item_offer.textViewEndHours
import kotlinx.android.synthetic.main.item_offer.textViewEndHoursJobs
import kotlinx.android.synthetic.main.item_offer.textViewFinishHours
import kotlinx.android.synthetic.main.item_offer.textViewFullName
import kotlinx.android.synthetic.main.item_offer.textViewSmallValue
import kotlinx.android.synthetic.main.item_offer.textViewStartHours
import kotlinx.android.synthetic.main.item_offer.textViewValue
import kotlinx.android.synthetic.main.item_offer_detail.*
import timber.log.Timber

class OfferDetailsActivity : AppCompatActivity() {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var token: String? = null
    private lateinit var offerDisposable: Disposable
    private lateinit var offerByIdDisposable: Disposable
    private lateinit var value: TextView
    private lateinit var offer: Offer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.item_offer_detail)
        initView()
        getData()


    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun getData() {
        offer = intent.getSerializableExtra(DataConstants.OFFER) as Offer
        getSharedPreferences()
        getDataById(offer.id)
    }

    private fun getDataById(id: String) {
        val getDataByIdService =
            ApiClient.getRetrofit(token).create(ApiService::class.java).getOfferById(
                id
            )

        offerByIdDisposable = getDataByIdService.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { response -> showAllDate(response) },
                { error -> handleError(error) }
            )
        compositeDisposable.add(offerByIdDisposable)

    }

    private fun showAllDate(response: Offer?) {
        // Set View depending of response
        Timber.e("showAllDate, response is  :$response")
    }

    private fun getSharedPreferences() {
        val prefs = this.getSharedPreferences(
            DataConstants.PREFERENCE_NAME,
            Context.MODE_PRIVATE
        )
        token = prefs.getString(DataConstants.AUTH_TOKEN, "")
        Timber.e("===", "OfferDetailsActivity" + prefs.getString(DataConstants.AUTH_TOKEN, ""))

    }

    private fun initView() {
        value = textViewValue
        var smallValue = textViewSmallValue
        var fullName: TextView = textViewFullName
        var city: TextView = textViewCity
        var district: TextView = textViewDistrict
        var hours: TextView = textViewStartHours
        var endHours: TextView = textViewEndHours
        var finishHours: TextView = textViewFinishHours
        var endHoursJobs: TextView = textViewEndHoursJobs
        var linearLayoutJobDetails: LinearLayout = linearLayoutJobDetails

        buttonCreate.setOnClickListener {
            createNewShift()
        }
    }

    private fun createNewShift() {
        val service = ApiClient.getRetrofit(token).create(ApiService::class.java).createNewOffer(
            CreateOfferRequest(1, true)
        )

        offerDisposable = service.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { response -> showDetails(response) },
                { error -> handleError(error) }
            )
        compositeDisposable.add(offerDisposable)
    }

    private fun showDetails(response: Void) {
        Timber.e(" New shift successfully created, status :$response")
        Snackbar.make(value, "Created", Snackbar.LENGTH_LONG)
            .show()
    }


    private fun handleError(error: Throwable) {
        Timber.e(" Not created , status :${error}")
        Snackbar.make(value, getString(R.string.default_error) + ": $error", Snackbar.LENGTH_LONG)
            .show()
    }
}


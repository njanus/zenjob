package com.nikola.zenjob.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.nikola.zenjob.R
import com.nikola.zenjob.app.DataConstants
import com.nikola.zenjob.network.ApiClient
import com.nikola.zenjob.network.request.UserRequest
import com.nikola.zenjob.network.response.Authorization
import com.nikola.zenjob.network.services.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import timber.log.Timber

class LoginActivity : AppCompatActivity() {

    private lateinit var username: EditText
    private lateinit var password: EditText
    private lateinit var login: Button
    private lateinit var loading: ProgressBar
    private lateinit var loginDisposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        initView()
        login.setOnClickListener {
            if (fieldsValidation() && mailValidation()) {
                startLogin()
            }
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        loginDisposable.dispose()
    }

    private fun initView() {
        username = editTextUsername
        password = editTextPassword
        login = buttonLogin
        loading = progressBarLoading

    }

    private fun fieldsValidation(): Boolean {
        var isValid = true
        if (username.text.isEmpty()) {
            username.error = getString(R.string.username_not_empty)
            isValid = false
        }
        if (password.text.isEmpty()) {
            password.error = getString(R.string.password_not_empty)
            isValid = false
        }
        return isValid
    }

    private fun mailValidation(): Boolean {
        if (!Patterns.EMAIL_ADDRESS.matcher(username.text.toString()).matches()) {
            username.error = getString(R.string.mail_not_valid_format)
            return false
        }
        return true
    }

    private fun startLogin() {

        loading.visibility = View.VISIBLE
        val loginService = ApiClient.getRetrofit("").create(ApiService::class.java).login(
            UserRequest(username.text.toString(), password.text.toString())
        )

        loginDisposable = loginService.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { response -> loginSuccessfully(response) },
                { error -> handleError(error) }
            )

    }

    private fun loginSuccessfully(response: Authorization?) {
        Timber.e(" Login Successfully :${response.toString()}")
        loading.visibility = View.GONE
        username.text.clear()
        password.text.clear()

        if (response != null) {
            savedData(response)
        }
        progressBarLoading.visibility = View.GONE
        openListActivity()
    }

    private fun savedData(response: Authorization?) {
        val token = response?.access_token
        val type = response?.token_type
        val refreshToken = response?.refresh_token
        val user = response?.username

        val editor =
            getSharedPreferences(DataConstants.PREFERENCE_NAME, Context.MODE_PRIVATE).edit()
        editor.putString(DataConstants.AUTH_TOKEN, token)
        editor.putString(DataConstants.REFRESH_TOKEN, refreshToken)
        editor.putString(DataConstants.TOKEN_TYPE, type)
        editor.putString(DataConstants.AUTH_USER, user)
        editor.apply()

        Timber.e("===", "Success authorization $token")
    }

    private fun openListActivity() {
        val intent = Intent(this, OfferActivity::class.java)
        startActivity(intent)
    }

    private fun handleError(error: Throwable?) {
        loading.visibility = View.GONE
        Snackbar.make(login, getString(R.string.default_error), Snackbar.LENGTH_LONG)
            .show()
        Timber.e(" Login error :$error")
    }

}


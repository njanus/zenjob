package com.nikola.zenjob.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.nikola.zenjob.R
import com.nikola.zenjob.adapter.OffersAdapter
import com.nikola.zenjob.app.DataConstants
import com.nikola.zenjob.network.ApiClient
import com.nikola.zenjob.network.response.*
import com.nikola.zenjob.network.services.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.progressBarLoading
import kotlinx.android.synthetic.main.activity_offer.*
import timber.log.Timber

class OfferActivity : AppCompatActivity(), OffersAdapter.OnAdapterListener {

    private var offers: ArrayList<Offer> = arrayListOf()
    private var token: String? = null
    private lateinit var loading: ProgressBar
    private lateinit var offerDisposable: Disposable
    private lateinit var offersAdapter: OffersAdapter
    private lateinit var recyclerViewOffers: RecyclerView
    private lateinit var jobs: TextView
    private lateinit var title: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_offer)
        initView()
        getSharedPreferences()


    }

    override fun onResume() {
        super.onResume()
        getOfferData()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (offerDisposable != null) {
            offerDisposable.dispose()
        }
    }

    private fun initView() {
        loading = progressBarLoading
        jobs = textViewJobs
        title = textViewTitle
        recyclerViewOffers = recyclerViewList
        recyclerViewOffers.layoutManager = LinearLayoutManager(this)
        offersAdapter = OffersAdapter(this, offers, this)
        recyclerViewOffers.adapter = offersAdapter

    }

    private fun getSharedPreferences() {
        val prefs = this.getSharedPreferences(
            DataConstants.PREFERENCE_NAME,
            Context.MODE_PRIVATE
        )
        token = prefs.getString(DataConstants.AUTH_TOKEN, "")
        Timber.e("===", "Offer activity" + prefs.getString(DataConstants.AUTH_TOKEN, ""))

    }

    private fun getOfferData() {

        loading.visibility = View.VISIBLE
        val offerService = ApiClient.getRetrofit(token).create(ApiService::class.java).getOffers(
        )

        offerDisposable = offerService.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { response -> showDetails(response) },
                { error -> handleError(error) }
            )


    }

    private fun showDetails(response: Offers) {
        offers.clear()
        Timber.e(" Offers data successfully :$response")
        loading.visibility = View.GONE
        val offersCount = response.offers.size
        jobs.text =
            String.format(this.getString(R.string.jobs_waiting), offersCount)
        offers.addAll(response.offers)
        textViewDummyData.visibility = View.GONE

    }


    private fun handleError(error: Throwable?) {
        loading.visibility = View.GONE
        jobs.text =
            String.format(this.getString(R.string.jobs_waiting), 0)
        Snackbar.make(title, getString(R.string.default_error) + ": $error", Snackbar.LENGTH_LONG)
            .show()
        Timber.e(" Offer get data error :$error")

        setDummyDate()
    }

    private fun setDummyDate() {
        offers.clear()
        textViewDummyData.visibility = View.VISIBLE
        val offerFirst = Offer(arrayListOf(),"", "Compane 100200bg", "Long destrption for company","14","100",
            10.0,"1", "nothing","",Location("Belgrade", "New Belgrade",42.10,22.10,"","","","","","")
            ,"", arrayListOf(), arrayListOf(), "Big Title"
        )

        val offerSecond = Offer(arrayListOf(),"", "Company 200100as", "Long destrption for company 2","24","200",
            30.0,"1", "nothing 2","",Location("Belgrade", "Old Town",42.10,23.10,"","","","","","")
            ,"", arrayListOf(), arrayListOf(), "Big Title 2"
        )

        val offersHelp: MutableList<Offer> = arrayListOf()
        offersHelp.add(offerFirst)
        offersHelp.add(offerSecond)
        offers.addAll(offersHelp)
        offersAdapter.notifyDataSetChanged()
    }

    override fun onMoreDetails(position: Int) {
        val offer = offers[position]
        val intent = Intent(this, OfferDetailsActivity::class.java)
        intent.putExtra(DataConstants.OFFER, offer)
        startActivity(intent)
    }

}


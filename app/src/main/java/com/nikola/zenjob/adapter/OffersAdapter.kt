package com.nikola.zenjob.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.nikola.zenjob.R
import com.nikola.zenjob.network.response.Offer


class OffersAdapter(
    private var context: Context,
    private var offers: MutableList<Offer>,
    private val onAdapterListener: OnAdapterListener

) :
    RecyclerView.Adapter<OffersAdapter.RateViewHolderItem>() {
    private lateinit var view: View

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): RateViewHolderItem {
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_offer, parent, false);
        return RateViewHolderItem(view);
    }


    inner class RateViewHolderItem internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var value: TextView = itemView.findViewById(R.id.textViewValue)
        var smallValue: TextView = itemView.findViewById(R.id.textViewSmallValue)
        var textViewFullName: TextView = itemView.findViewById(R.id.textViewFullName)
        var textViewCity: TextView = itemView.findViewById(R.id.textViewCity)
        var textViewDistrict: TextView = itemView.findViewById(R.id.textViewDistrict)
        var textViewStartHours: TextView = itemView.findViewById(R.id.textViewStartHours)
        var textViewEndHours: TextView = itemView.findViewById(R.id.textViewEndHours)
        var textViewFinishHours: TextView = itemView.findViewById(R.id.textViewFinishHours)
        var textViewEndHoursJobs: TextView = itemView.findViewById(R.id.textViewEndHoursJobs)
        var linearLayoutJobDetails: LinearLayout =
            itemView.findViewById(R.id.linearLayoutJobDetails)

    }

    override fun getItemCount(): Int {
        return offers.size
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: RateViewHolderItem, position: Int) {
        val offer: Offer = offers[position]
        holder.smallValue.text = offer.earningHourly
        holder.value.text = offer.earningTotal
        holder.textViewCity.text = offer.location.city
        holder.textViewDistrict.text = offer.location.district
        holder.textViewFullName.text = offer.description
        holder.smallValue.text = offer.title
        holder.linearLayoutJobDetails.setOnClickListener { onAdapterListener.onMoreDetails(position) }

    }

    interface OnAdapterListener {
        fun onMoreDetails(position: Int)
    }

}

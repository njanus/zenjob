package com.nikola.zenjob.app

import android.app.Application

import com.nikola.zenjob.BuildConfig
import timber.log.Timber


class ZenjobApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

    }

}
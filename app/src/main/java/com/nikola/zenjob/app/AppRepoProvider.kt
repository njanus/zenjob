package com.nikola.zenjob.app

import com.nikola.zenjob.network.request.UserRequest
import com.nikola.zenjob.network.response.Authorization
import com.nikola.zenjob.network.services.ApiService
import io.reactivex.Single


object AppRepoProvider {


    fun login(service: ApiService, user: UserRequest): Single<Authorization> {
        return service.login(user)
    }

}
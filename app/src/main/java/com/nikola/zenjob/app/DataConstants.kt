package com.nikola.zenjob.app

class DataConstants {
    companion object {
        const val SPLASH_DELAY_TIME: Long = 2500 //2.5 seconds
        const val PREFERENCE_NAME: String = "zenjob"
        const val AUTH_TOKEN: String = "token" // save token
        const val AUTH_USER: String = "user" // save user
        const val REFRESH_TOKEN: String = "refresh_token" // save refresh token
        const val TOKEN_TYPE: String = "token_type"
        const val OFFER: String = "offer"

    }

}
package com.nikola.zenjob.utils

import android.content.Context
import androidx.swiperefreshlayout.widget.CircularProgressDrawable

/** This is class is used for different utils methods */

class MiscUtil {

    companion object {

        fun getProgressLoadingBar(context: Context): CircularProgressDrawable {
            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            return circularProgressDrawable
        }
    }
}